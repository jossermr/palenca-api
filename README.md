# Palenca API

## Prerequisites
The setups steps expect following tools installed on the system.
 - Git
 - Ruby 3.0.2
 - Rails 6.1.1

### Install gems
``` bash
bundle install
```

### Create and setup the database

Run the following commands to create and setup the database.

``` bash
rake db:create
rake db:migrate
rake db:sedd
```

### Start the Rails server

You can start the rails server using the command given below.

``` bash
rails s
```

And now you can visit the site with the URL http://localhost:3000
