# frozen_string_literal: true

# Create users migration
class CreateUsers < ActiveRecord::Migration[6.1]
  def change
    create_table :users do |t|
      t.string :name
      t.string :last_name
      t.string :phone
      t.string :email
      t.string :password
      t.string :access_token

      t.timestamps
    end
  end
end
