# frozen_string_literal: true

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

User.create(
  name: 'Pierre',
  last_name: 'Palenca',
  phone: '525511223344',
  email: 'pierre@palenca.com',
  password: 'MyPwdChingon123',
  access_token: 'cTV0aWFuQ2NqaURGRE82UmZXNVBpdTRxakx3V1F5'
)
