# frozen_string_literal: true

# Manages requests for stores.
module Api
  # Health check for Palenca API
  class HealthCheckController < ApplicationController
    # Essentially, used to respond to health checks for infrastructure management
    # purposes.
    def hello
      render json: { response: 'Hello Palenca' }, status: :ok
    end
  end
end
