# frozen_string_literal: true

module Api
  module V1
    # User controller
    class UsersController < ApplicationController

      # POST /api/v1/uber/login
      def login
        user = User.find_by!(email: params[:email], password: params[:password])
        render json: {
          access_token: user.access_token
        }, status: :ok
      end

      # POST /api/v1/uber/get-profile/:access_token
      def profile
        user = User.find_by!(access_token: params[:access_token])
        render json: user, status: :ok
      end
    end
  end
end
