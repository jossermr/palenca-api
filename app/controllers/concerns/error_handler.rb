# frozen_string_literal: true

# Try to catch an error and return it as JSON.
module ErrorHandler
  extend ActiveSupport::Concern

  included do
    rescue_from ActiveRecord::RecordInvalid do |e|
      render json: {
        message: 'There was a problem trying to save the user. Check the request and try again.',
        errors: e.message,
        log_id: log_id
      }, status: :bad_request
    end
    rescue_from ActiveRecord::RecordNotSaved do |e|
      render json: {
        message: 'There was a problem trying to save the user. Try again.',
        errors: e.message,
        log_id: log_id
      }, status: :unprocessable_entity
    end
    rescue_from ActiveRecord::RecordNotDestroyed do |e|
      render json: {
        message: 'There was a problem trying to save the user. Try again.',
        errors: e.message,
        log_id: log_id
      }, status: :unprocessable_entity
    end
    rescue_from ActiveRecord::RecordNotFound do |e|
      render json: {
        message: 'There was a problem trying to retrieve the user.',
        errors: e.message,
        log_id: log_id
      }, status: :bad_request
    end
    rescue_from ActionController::ParameterMissing do |e|
      render json: {
        message: 'There was a problem trying with the parameters.',
        errors: e.message,
        log_id: log_id
      }, status: :bad_request
    end
    rescue_from ArgumentError do |e|
      render json: {
        message: 'There was a problem trying to create a user.',
        errors: e.message,
        log_id: log_id
      }, status: :bad_request
    end
  end

  # Set a log id and send message to New Relic monitor.
  #
  # @return [String] Log ID.
  def log_id
    SecureRandom.uuid
  end
end
