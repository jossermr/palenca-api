# frozen_string_literal: true

# A User represents a person.
#
# This object is used to store the information like full name, phone and email.
class User < ApplicationRecord
  before_save :downcase_fields

  # @!attribute [rw] name
  # @return [String]

  # @!attribute [rw] last_name
  # @return [String]

  # @!attribute [rw] phone
  # @return [String]

  # @!attribute [rw] email
  # @return [String]

  # @!attribute [rw] password
  # @return [String]

  validates :name,
            presence: true
  validates :last_name,
            presence: true
  validates :email,
            presence: true
  validates :password,
            presence: true,
            length: { minimum: 5, maximum: 15 }
  validates :phone,
            allow_blank: true,
            numericality: true,
            length: { minimum: 10, maximum: 15 }

  def downcase_fields
    email.downcase!
  end
end
