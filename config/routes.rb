# frozen_string_literal: true

Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  namespace 'api' do
    namespace 'v1' do
      post '/uber/login', to: 'users#login'
      get '/uber/get-profile/:access_token', to: 'users#profile'
    end
    get '/hello', to: 'health_check#hello'
  end
end
